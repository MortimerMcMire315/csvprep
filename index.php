<?php
/*
Chris Murad
6/29/2016
CSV Parse script
Last Updated: 10/23/2017
*/
//define('CLI_SCRIPT', true);


//Build as local Moodle Plugin
require_once("../../config.php");
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/authlib.php');
require_once('lib.php');
$url = new moodle_url('/local/csvprep/index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');
$PAGE->set_title('CSV PREP');
$PAGE->set_heading('CSV PREP');
$PAGE->navbar->add('CSV PREP', $url);
$PAGE->requires->jquery();
require_login();
require_capability('moodle/site:uploadusers', context_system::instance());
$exitmessage = "Your CSV has been converted and is ready for HR Import - Redirecting you to the HR Import page";


echo $OUTPUT->header();


$mylogloc =$CFG->dataroot .'/csv/csv/';
$dateforlog = date("Y/m/d h:i:s",  time());
$csvfile ='/mnt/cifs/UPSFTP/ReportView-TotaraCSV.csv';


//If Log Exists Move it
if( file_exists($mylogloc . 'csvlog.txt') ) {
    rename($mylogloc . 'csvlog.txt', $CFG->dataroot . '/csv/csv/store/' . time() . "csvlog.txt" );
}

//Logging Function
logToFile($mylogloc .'csvlog.txt', "*****SCRIPT CALLED FROM WEB******");


//Pull Data from Database
$pullfromdb = pullFromDB();

//Build CSV
$buildcsv = buildCSV($pullfromdb,$csvfile,$mylogloc);

//Good bye


logToFile($mylogloc .'csvlog.txt', "*****WEB SCRIPT COMPLETED******");
redirect($CFG->wwwroot.'/admin/tool/totara_sync/admin/syncexecute.php', $exitmessage, null, \core\output\notification::NOTIFY_SUCCESS);
echo $OUTPUT->footer();


