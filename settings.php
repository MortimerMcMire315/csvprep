<?php
defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $ADMIN->add('root', new admin_externalpage('local_csvprep', 'CSV PREP', new moodle_url('/local/csvprep/index.php')));
}