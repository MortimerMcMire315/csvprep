<?php
/*
Chris Murad
6/29/2016
CSV Parse script
Last Updated: 10/23/2017
*/
define('CLI_SCRIPT', true);
//Build as local Moodle Plugin
require_once(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/adminlib.php');
//require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/user/lib.php');
//require_once($CFG->dirroot.'/user/profile/lib.php');
require_once('lib.php');
$mylogloc = $CFG->dataroot .'/csv/csv/';
$dateforlog = date("Y/m/d h:i:s",  time());
$csvfile ='/mnt/cifs/UPSFTP/ReportView-TotaraCSV.csv';
//$csvfile ='newtotara.csv';
//If Log Exists Move it
if( file_exists($mylogloc . 'csvlog.txt') ) {
    rename($mylogloc . 'csvlog.txt', $CFG->dataroot . '/csv/csv/store/' . time() . "csvlog.txt" );
}

//Logging Function
logToFile($mylogloc .'csvlog.txt', "*****CLI SCRIPT CALLED******");


//Pull Data from Database
$pullfromdb = pullFromDB();

//Build CSV
$buildcsv = buildCSV($pullfromdb,$csvfile,$mylogloc);

//Good bye

logToFile($mylogloc .'csvlog.txt', "*****CLI SCRIPT COMPLETED******");
echo $OUTPUT->footer();

