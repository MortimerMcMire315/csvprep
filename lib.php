<?php
/*
Chris Murad
6/29/2016
CSV Parse script
Last Updated: 10/23/2017
*/
//including parsecsv class: https://github.com/parsecsv/parsecsv-for-php
//require_once 'csvprep.php';
require_once(__DIR__."../../../config.php");
require_once 'parsecsv.lib.php';

function logToFile($filename, $msg){
    $fd = fopen($filename, "a");
    $str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg;
    fwrite($fd, $str . "\n");
    fclose($fd);
}

function pullFromDB(){
    global $CFG, $DB;
    $pullfromdb = array();
    $sql = "SELECT username,idnumber AS 'EE NUMBER', firstname as 'FIRST NAME', email FROM mdl_user WHERE deleted=0";
    $result = $DB->get_records_sql($sql, array());
    $usersarray = json_decode(json_encode($result), true);
    foreach($usersarray as $key){
            $pullfromdb[]=$key;
    }

    return $pullfromdb;
}

function buildCSV($pullfromdb,$csvfile,$mylogloc){
    global $CFG;
    //Creating the new object
    $csv = new parseCSV();
    $csv->delimiter = ",";
//start
//parse the actual CSV
    $csv ->parse($csvfile);
//$csv->parse('testing.csv');

    fixArrayKey($csv->data);

//Start Writing and building the CSV
    $filedir = $CFG->dataroot .'/csv/csv/ready/';


//These two files will be identical.
    $file=fopen($filedir . 'user.csv','w');

//We need headings
    $header = array();
    foreach($csv->data[1] as $key=>$val){
        $header[]=$key;
    }

    $header2 = array();
    foreach($pullfromdb[0] as $key=>$val){
        $header2[]=$key;
    }

//Make them lowercase
    $h1 = array();
    foreach($header as $k){
        $h1[] = strtolower($k);

    }
    $h2 = array();
    foreach($header2 as $k){
        $h2[] = strtolower($k);
    }

//Make one Array of the headers
    $mergedata = array_merge($h1,$h2);
    $hmerge = array_unique($mergedata);

//Make all data lowercase
    $filtercsvdata = array();
    foreach($csv->data as $row){
        $r=array();
        foreach($row as $key=>$val){
            $r[strtolower($key)] = $val;
        }

        $filtercsvdata[] = $r;

    }
    foreach($pullfromdb as $row){
        $r=array();
        foreach($row as $key=>$val){
            $r[strtolower($key)] = $val;
        }
        $filtercsvdata[] = $r;
    }

/**
 * $filtercsvdata structure at this point: Combined LDAP and Totara DB fields
 * [
 *   0   => ["ee number" => 1, "other LDAP fields" => "somestuff" ... ]
 *   ...
 *   More fields from the LDAP CSV
 *   ...
 *   936 => ["ee number" => 1, "username" => "someuser", "email" => "myemail@example.com", "first name" => "my first name"]
 *   ...
 * ]
 *
 * $hmerge structure at this point:
 * (The items are not necessarily in this order, but the order is unimportant.)
 *
 * [
 *   0 => "ee number"
 *   1 => "first name"
 *   2 => "last name"
 *   ...
 * ]
 */
//combine arrays!
    $c = array();
    foreach ($filtercsvdata as $rec){
        // $rec is either a row from LDAP or from the Totara DB.
        // Both must have "ee number".
        //
        // $c is a new array representing ALL of the combined data, this time
        // with the "ee number"s used as array keys. Each item in $c is a row.
        //
        // So for *each* $rec (record), we run through *all* the possible column
        // names, checking if that column name exists as a key in the record.
        // If it does exist, we add that key/value pair to the array in $c whose
        // key corresponds to the record's "ee number".
        //
        $i = array();
        foreach ($hmerge as $key){
            if (array_key_exists($key, $rec) ) {
                $c[$rec["ee number"]][$key] = $rec[$key];
            }
        }
    }

    foreach ($c as $eenum => $val) {

        //use preferred first name

        if (array_key_exists('preferred first name', $val)){
            $val['first name'] = $val['preferred first name'];
        }

        //Put in blanks if there is nothing
        foreach ($hmerge as $key){
            if (!array_key_exists($key, $val) ) {
                logToFile($mylogloc .'csvlog.txt', print_r($val["ee number"] . " " . $val["first name"] . " " . $val["last name"]  . " ". $key . " is Blank!", true));
                $c[$val["ee number"]][$key] = "";
            }

        }

    }
//Begining of the end
    $final = array();


//Cleaning up
    foreach ($c as $last){

// Strip Time off of date fields:
        $dateleng = strlen($last["last hire date"]);
        $dateleng2 = strlen($last["date in job"]);
        $termdatelen = strlen($last["termination date"]);

        if ($dateleng >1){
            $lhdtodate = strtotime($last["last hire date"]);
            $lhdnewformat = date('m/d/Y',$lhdtodate);
            $last["last hire date"] = $lhdnewformat;
        }

        if ($dateleng2 >1){
            $dijtodate = strtotime($last["date in job"]);
            $dijnewformat = date('m/d/Y',$dijtodate);
            $last["date in job"] = $dijnewformat;
        }

        if ($termdatelen >1){
            $termtodate = strtotime($last["termination date"]);
            $termnewformat = date('m/d/Y',$termtodate);
            $last["termination date"] = $termnewformat;
        }

//Check if CEO
        if ($last["job code"] == "13CEO" || $last["job code"] == "5000"){
            $last["supervisor emplid"] =  "";
        }


// Check if Account is Suspended
        if ($last["ee status"] == "T" || $last["ee status"] == "R" || $last["ee status"] == "S" ){
            $last["ee status"] = "1";
        }else{
            $last["ee status"] ="0";
        }

        //Set Flags for Supervisor / Driver
        if ($last["is supervisor"] == "Y"){
            $last["is supervisor"] ="1";
        }else{
            $last["is supervisor"] ="0";
        }

        if ($last["driver status"] == "A"){
            $last["driver status"] = "1";
        }else {
            $last["driver status"] = "0";
        }
//Only have one First Name Field
        if(array_key_exists('preferred first name', $last)){
            unset($last['preferred first name']);
        }
        //Remove Division column because Department will map back
        if(array_key_exists('division', $last)){
            unset($last['division']);
        }

        $final[] = $last;
    }

//HR Import Requires Time Modified to be 0 to run the record on each load
    foreach ($final as &$timemod) {
        $timemod['timemodified'] = 0;
    }

    // After all the other values in this row have been merged into our
    // master array, we add these two in order to assign the job with id=1 to
    // each of our users.
    foreach ($final as &$jobrow) {
        $jobrow['jobassignmentidnumber'] = 1;
        $jobrow['managerjobassignmentidnumber'] = 1;
    }

    $finalh=array();

//Only have one First Name Field
    foreach ($hmerge as $last =>$v){

        if($v == 'preferred first name'){
            unset($hmerge[$last]);
        }

        //Remove Division column because Department will map back

        if($v == 'division'){
            unset($hmerge[$last]);
        }

    }

//create new column headings
    $hmerge[18]= "timemodified";
    $hmerge[]= "jobassignmentidnumber";
    $hmerge[]= "managerjobassignmentidnumber";

//Remove users not in file but in DB - Pull Problem users
    foreach ($final as $key => $val) {
        if ($val["last name"] == ""){
            logToFile($mylogloc .'csvlog.txt', print_r("Last name is blank for: " . $val["ee number"] . " " . $val["first name"] . " Removing!", true));

            unset($final[$key]);
        }

    }

    //Write column names into the CSV
    fputcsv($file,$hmerge);

    //Write each row into the CSV
    foreach ($final as $row) {
        fputcsv($file, $row);
    };
    fclose($file);

    // Copy
    copy($filedir . 'user.csv', $filedir . 'jobassignment.csv');
}
//clean array keys of special char. / prep for hr import
function fixArrayKey(&$arr)
{
    $arr = array_combine(
        array_map(
            function ($str) {
                return preg_replace('/[^A-Za-z0-9\. -]/', '', $str);
            },
            array_keys($arr)
        ),
        array_values($arr)
    );

    foreach ($arr as $key => $val) {
        if (is_array($val)) {
            fixArrayKey($arr[$key]);
        }
    }
}
